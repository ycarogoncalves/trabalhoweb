var url1 = "http://104.236.122.55:80/doctum/pw/tp1/route.php";
var url2 = "http://104.236.122.55:80/doctum/pw/tp1/route.php/?id=";

function alertSuccess( msg ){
	$('#alertSuccess').toggleClass('active');
	$('#alertSuccess').text(msg);

	setTimeout(function(){
		$('#alertSuccess').toggleClass('active')
	},5000);
}

function alertError( msg ){
	$('#alertError').toggleClass('active');
	$('#alertError').text(msg);

	setTimeout(function(){
		$('#alertError').toggleClass('active')
	},5000);
}

function atualizar( id, data, table ) {
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url: url2+id,
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
		alertSuccess('Cadastro Atualizado!');
	}).error(function( error ){
		alertError('Erro ao Atualizar');
	});
}

function salvar( data, table ){
	$.ajax({
		type: 'POST',
		contentType: 'json',
		cache: false,
		url: url1,
		headers: {'table': table},
		data: data

	}).success(function( data ){
		console.log(data);
		alertSuccess('Cadastro efetuado!');
	}).error(function( error ){
		console.log(error);
		alertError('Erro ao cadastrar');
	});
}

function excluir( id, table ) {
	var deletar = confirm("Deseja realmente excluir este registro?");
	if(deletar) {
		$.ajax({
			type: 'DELETE',
			contentType: 'json',
			cache: false,
			url: url2+id,
			headers: {'table': table}

		}).success(function( data ){
			console.log(data);
			alertSuccess('Sucesso ao excluir.');
		}).error(function( error ){
			console.log(error);
			alertError('Erro ao excluir');
		});
	}
}

$('.cad').on('click',function(){
	document.getElementById("form").reset();
});