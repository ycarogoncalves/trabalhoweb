$.getScript("js/functions.js",function(data){console.log('functions.js carregado')});

function editarFunc( id ) {
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table':'funcionario'},

	}).success(function( data ){
		$('#idFunc').val(data[0].id);
		$('#nameFunc').val(data[0].nome);
		$('#funcaoFunc').val(data[0].funcao);
		$('#teleFixoFunc').val(data[0].telefone_fixo);
		$('#teleMovelFunc').val(data[0].telefone_movel);
		$('#emailFunc').val(data[0].email);
	}).error(function( error ){
		console.log(error);
	});

}
function excluirFunc( id ){
	excluir(id,"funcionario");
	atualizarTabelaFuncionarios();
}

function atualizarTabelaFuncionarios(){
	document.getElementById("form").reset();
	var tabela = document.getElementById('dados-funcionarios');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'funcionario'}
	}).success(function( funcionarios ){
		for (var i = 0; i<funcionarios.length;i++){
			if( funcionarios[i].email != null ){
				var linha=document.createElement("tr");
				var cid=document.createElement("td");
				var cnome=document.createElement("td");
				var cfuncao=document.createElement("td");
				var ctelefoneFixo=document.createElement("td");
				var ctelefoneMovel=document.createElement("td");
				var cemail=document.createElement("td");
				var cbuttons=document.createElement("td");

				cbuttons.setAttribute('class','buttons');

				var vid= document.createTextNode(funcionarios[i].id);
				var vnome= document.createTextNode(funcionarios[i].nome);
				var vfuncao= document.createTextNode(funcionarios[i].funcao);
				var vteleFixo= document.createTextNode(funcionarios[i].telefone_fixo);
				var vteleMovel= document.createTextNode(funcionarios[i].telefone_movel);
				var vemail=document.createTextNode(funcionarios[i].email);

				var btnEdit = document.createElement('Button');
				btnEdit.setAttribute('type','button');
				btnEdit.setAttribute('class','btn btn-primary');
				btnEdit.setAttribute('id','editFunc');
				btnEdit.setAttribute('onclick','editarFunc(value)');
				btnEdit.setAttribute('data-toggle','modal');
				btnEdit.setAttribute('data-target','#exampleModal');
				btnEdit.setAttribute('value',funcionarios[i].id);
				btnEdit.title = 'Editar';
				btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

				var btnEx = document.createElement('Button');
				btnEx.setAttribute('type','button');
				btnEx.setAttribute('class','btn btn-danger');
				btnEx.setAttribute('id','excluirFunc');
				btnEx.setAttribute('onclick','excluirFunc(value)');
				btnEx.setAttribute('value',funcionarios[i].id);
				btnEx.title = 'Excluir';
				btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

				cid.appendChild(vid);
				cnome.appendChild(vnome);
				cfuncao.appendChild(vfuncao);
				ctelefoneFixo.appendChild(vteleFixo);
				ctelefoneMovel.appendChild(vteleMovel);
				cemail.appendChild(vemail);
				cbuttons.appendChild(btnEdit);
				cbuttons.appendChild(btnEx);

				linha.appendChild(cid);
				linha.appendChild(cnome);
				linha.appendChild(cfuncao);
				linha.appendChild(ctelefoneFixo);
				linha.appendChild(ctelefoneMovel);
				linha.appendChild(cemail);
				linha.appendChild(cbuttons);
				tabela.appendChild(linha);
			}
		}
	}).error(function( error ){
		console.log(error);
	});
	
}

$("#salvarFunc").on('click',function(){
	if( $("#idFunc").val() != "" ) {
		var data = JSON.stringify({ 
			id: $("#idFunc").val(),
		    nome: $("#nameFunc").val(), 
		    funcao: $("#funcaoFunc").val(), 
		    email: $("#emailFunc").val(), 
		    telefone_movel: $("#teleMovelFunc").val(), 
		    telefone_fixo: $("#teleFixoFunc").val()
	    });

		atualizar(data.id, data, "funcionario");
		atualizarTabelaFuncionarios();
	} else {
		var data = JSON.stringify({ 
		    nome: $("#nameFunc").val(), 
		    funcao: $("#funcaoFunc").val(), 
		    email: $("#emailFunc").val(), 
		    telefone_movel: $("#teleMovelFunc").val(), 
		    telefone_fixo: $("#teleFixoFunc").val()
	    });
		
		salvar(data, "funcionario");
		atualizarTabelaFuncionarios();
	}
});

$("#atualizar-func").on('click',function(){
	atualizarTabelaFuncionarios();
});


$(document).ready(function(){
	atualizarTabelaFuncionarios();
});