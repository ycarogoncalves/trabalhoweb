$.getScript("js/functions.js",function(data){console.log('functions.js carregado')});

$("#salvarMotoristas").on('click',function(){
	if( $("#idMotoristas").val() != "" ) {
		var data = JSON.stringify({ 
			id: $("#idMotoristas").val(),
		    nome: $("#nameMotoristas").val(), 
		    email: $("#emailMotoristas").val(), 
		    telefone_movel: $("#teleMovelMotoristas").val(), 
		    telefone_fixo: $("#teleFixoMotoristas").val()
	    });
	    
		atualizar( data.id, data, "motorista" )
	} else {
		var data = JSON.stringify({ 
		    nome: $("#nameMotoristas").val(), 
		    funcao: $("#funcaoMotoristas").val(), 
		    email: $("#emailMotoristas").val(), 
		    telefone_movel: $("#teleMovelMotoristas").val(), 
		    telefone_fixo: $("#teleFixoMotoristas").val()
	    });
		
		salvar(data, "motorista")
	}
});

function editarMotorista( id ) {
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: url2+id,
		headers: {'table':'motorista'},

	}).success(function( data ){
		console.log(data);
		document.getElementById('idMotoristas').value = data[0].id;
		document.getElementById('nameMotoristas').value = data[0].nome;
		document.getElementById('teleFixoMotoristas').value = data[0].telefone_fixo;
		document.getElementById('teleMovelMotoristas').value = data[0].telefone_movel;
		document.getElementById('emailMotoristas').value = data[0].email;

	}).error(function( error ){
		console.log(error);
	});

}
function excluirMotorista( id ) {
	excluir(id,'motorista');
}

function atualizarTabelaMotorista(){
	document.getElementById("form").reset();
	var tabela = document.getElementById('dados-motorista');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'motorista'},

	}).success(function( motoristas ){
		for (var i = 0; i<motoristas.length;i++){
			if( motoristas[i].email != null ){
				var linha=document.createElement("tr");
				var cid=document.createElement("td");
				var cnome=document.createElement("td");
				var ctelefoneFixo=document.createElement("td");
				var ctelefoneMovel=document.createElement("td");
				var cemail=document.createElement("td");
				var cbuttons=document.createElement("td");


				var vid= document.createTextNode(motoristas[i].id);
				var vnome= document.createTextNode(motoristas[i].nome);
				var vteleFixo= document.createTextNode(motoristas[i].telefone_fixo);
				var vteleMovel= document.createTextNode(motoristas[i].telefone_movel);
				var vemail=document.createTextNode(motoristas[i].email);

				var btnEdit = document.createElement('Button');
				btnEdit.setAttribute('type','button');
				btnEdit.setAttribute('class','btn btn-primary');
				btnEdit.setAttribute('id','editMotorista');
				btnEdit.setAttribute('onclick','editarMotorista('+motoristas[i].id+')');
				btnEdit.setAttribute('data-toggle','modal');
				btnEdit.setAttribute('data-target','#exampleModal');
				btnEdit.title = 'Editar';
				btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

				var btnEx = document.createElement('Button');
				btnEx.setAttribute('type','button');
				btnEx.setAttribute('class','btn btn-danger');
				btnEx.setAttribute('id','excluirMotorista');
				btnEx.setAttribute('onclick','excluirMotorista('+motoristas[i].id+')');
				btnEx.title = 'Excluir';
				btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

				cid.appendChild(vid);
				cnome.appendChild(vnome);
				ctelefoneFixo.appendChild(vteleFixo);
				ctelefoneMovel.appendChild(vteleMovel);
				cemail.appendChild(vemail);
				cbuttons.appendChild(btnEdit);
				cbuttons.appendChild(btnEx);

				linha.appendChild(cid);
				linha.appendChild(cnome);
				linha.appendChild(ctelefoneFixo);
				linha.appendChild(ctelefoneMovel);
				linha.appendChild(cemail);
				linha.appendChild(cbuttons);
				tabela.appendChild(linha);
			}
		}
	}).error(function( error ){
		console.log(error);
	});
	
}

$("#atualizar-motorista").on('click',function(){
	atualizarTabelaMotorista();
});

$(document).ready(function(){
	atualizarTabelaMotorista();
});