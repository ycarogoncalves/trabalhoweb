$.getScript("js/functions.js",function(data){console.log('functions.js carregado')});

var motoristas = {};
var veiculos = {};
var funcionarios = {};

function getAllMotoristas(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'motorista'},

	}).success(function( data ){
		motoristas = data;
	}).error(function( error ){
		console.log(error);
	});
}
function getAllCarros(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'veiculo'},

	}).success(function( data ){
		veiculos = data;
	}).error(function( error ){
		console.log(error);
	});
}
function getAllFuncionarios(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'funcionario'},

	}).success(function( data ){
		funcionarios = data;
	}).error(function( error ){
		console.log(error);
	});
}

function editarViagem( id ){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: url2+id,
		headers: {'table':'viagem'},

	}).success(function( data ){
		var idFunc = new Array();
		for(var i=0; i<data[0].funcionarios.length; i++){
			idFunc.push(data[0].funcionarios[i].funcionario__id);
		}
		$('#idViagem').val(data[0].id),
		$("#selectMotoristas").val(data[0].motorista), 
	    $("#selectCarros").val(data[0].carro), 
	    $("#saida").val(data[0].saida), 
	    $("#retorno").val(data[0].retorno),
	    $("#selectFuncionarios").val(idFunc)
	}).error(function( error ){
		console.log(error);
	});
}

function excluirViagem( id ){
	excluir(id,'viagem');
}

function atualizarTabelaViagem(){
	document.getElementById("form").reset();

	var tabela = document.getElementById('dados-viagem');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'viagem'},

	}).success(function( viagens ){
		for (var i = 0; i<viagens.length;i++){
			var linha=document.createElement("tr");
			var cid=document.createElement("td");
			var cmotorista=document.createElement("td");
			var ccarro=document.createElement("td");
			var cfuncioarios=document.createElement("td");
			var csaida=document.createElement("td");
			var cretorno=document.createElement("td");
			var cbuttons=document.createElement("td");

			var vid = document.createTextNode(viagens[i].id);
			var vmotorista = document.createTextNode(viagens[i].motorista__nome);
			var vcarro = document.createTextNode(viagens[i].carro__modelo);
			var nomes = "";
			for(var c = 0; c<viagens[i].funcionarios.length; c++){
				nomes = nomes+ viagens[i].funcionarios[c].funcionario__nome+", ";
			}
			var vfuncioarios = document.createTextNode(nomes);
			var vsaida = document.createTextNode(viagens[i].saida);
			var vretorno = document.createTextNode(viagens[i].retorno);

			var btnEdit = document.createElement('Button');
			btnEdit.setAttribute('type','button');
			btnEdit.setAttribute('class','btn btn-primary');
			btnEdit.setAttribute('id','editViagem');
			btnEdit.setAttribute('onclick','editarViagem('+viagens[i].id+')');
			btnEdit.setAttribute('data-toggle','modal');
			btnEdit.setAttribute('data-target','#exampleModal');
			btnEdit.title = 'Editar';
			btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

			var btnEx = document.createElement('Button');
			btnEx.setAttribute('type','button');
			btnEx.setAttribute('class','btn btn-danger');
			btnEx.setAttribute('id','excluirViagem');
			btnEx.setAttribute('onclick','excluirViagem('+viagens[i].id+')');
			btnEx.title = 'Excluir';
			btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

			cid.appendChild(vid);
			cmotorista.appendChild(vmotorista);
			ccarro.appendChild(vcarro);
			csaida.appendChild(vsaida);
			cfuncioarios.appendChild(vfuncioarios);
			cretorno.appendChild(vretorno);
			cbuttons.appendChild(btnEdit);
			cbuttons.appendChild(btnEx);

			linha.appendChild(cid);
			linha.appendChild(cmotorista);
			linha.appendChild(ccarro);
			linha.appendChild(cfuncioarios);
			linha.appendChild(csaida);
			linha.appendChild(cretorno);
			linha.appendChild(cbuttons);
			tabela.appendChild(linha);
		}
	}).error(function( error ){
		console.log(error);
	});
	
	
}

function carregarSelects(){
	$('#selectMotoristas').empty();
	$('#selectCarros').empty();
	$('#selectFuncionarios').empty();
	for(var i = 0; i<motoristas.length;i++){
		var option = document.createElement('option');
		$(option).attr({value: motoristas[i].id});
		$(option).append( motoristas[i].nome );

		$('#selectMotoristas').append(option);
	}
	for(var i = 0; i<veiculos.length;i++){
		var option = document.createElement('option');
		$(option).attr({value: veiculos[i].id});
		$(option).append( veiculos[i].modelo );

		$('#selectCarros').append(option);
	}

	for(var i = 0; i<funcionarios.length;i++){
		var option = document.createElement('option');
		$(option).attr({value: funcionarios[i].id});
		$(option).append( funcionarios[i].nome );

		$('#selectFuncionarios').append(option);
	}

}


$("#atualizar-viagem").on('click',function(){
	atualizarTabelaViagem();
});

$("#salvarViagem").on('click',function(){
	if( $("#idViagem").val() != "" ) {
		var data = JSON.stringify({ 
			id: $("#idViagem").val(),
		    motorista: parseInt( $("#selectMotoristas").val() ), 
		    carro: parseInt( $("#selectCarros").val() ), 
		    saida: $("#saida").val(), 
		    retorno: $("#retorno").val(),
		    funcionarios: $("#selectFuncionarios").val()
	    });

		atualizar( data.id, data, 'viagem' );
		atualizarTabelaViagem();
	} else {
		var data = JSON.stringify({ 
		    motorista: $("#selectMotoristas").val(), 
		    carro: $("#selectCarros").val(), 
		    saida: $("#saida").val(), 
		    retorno: $("#retorno").val(),
		    funcionarios: $("#selectFuncionarios").val()
	    });

		salvar(data, "viagem");
		atualizarTabelaViagem();
	}
});


$(document).ready(function(){
	getAllMotoristas();
	getAllCarros();
	getAllFuncionarios();

	setTimeout(carregarSelects,2000);
	atualizarTabelaViagem();
});