$.getScript("js/functions.js",function(data){console.log('functions.js carregado')});

function editarVeiculo( id ) {
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: 'http://104.236.122.55:80/doctum/pw/tp1/route.php/?id='+id,
		headers: {'table':'veiculo'},

	}).success(function( data ){
		$('#idVeiculo').val(data[0].id);
		$('#modelo').val(data[0].modelo);
		$('#qtdePessoa').val(data[0].qtde_passageiros);
		$('#ativo').prop("checked",parseInt( data[0].ativo));
		
	}).error(function( error ){
		console.log(error);
	});

}

function excluirVeiculo( id ){
	excluir(id, "veiculo");
}

$("#salvarVeiculo").on('click',function(){
	if( $("#idVeiculo").val() != "" ) {
		var data = JSON.stringify({ 
			id: $("#idVeiculo").val(),
		    modelo: $("#modelo").val(), 
		    qtde_passageiros: parseInt($("#qtdePessoa").val()),
		    ativo: document.getElementById('ativo').checked 
	    });

		atualizar(data.id, data, "veiculo");
		atualizarTabelaVeiculos();
	} else {
		var data = JSON.stringify({ 
		    modelo: $("#modelo").val(), 
		    qtde_passageiros: parseInt($("#qtdePessoa").val()), 
		    ativo:  document.getElementById('ativo').checked  
	    });

		salvar(data, "veiculo");
		atualizarTabelaVeiculos();
	}
});

function atualizarTabelaVeiculos(){
	document.getElementById("form").reset();
	var tabela = document.getElementById('dados-veiculo');
	tabela.innerHTML = "";
	$.ajax({
		type: 'GET',
		dataType: 'json',
		cache: false,
		url: url1,
		headers: {'table':'veiculo'}
	}).success(function( veiculos ){
		for (var i = 0; i<veiculos.length;i++){
			var linha=document.createElement("tr");
			var cid=document.createElement("td");
			var cmodelo=document.createElement("td");
			var cqtdeP=document.createElement("td");
			var cativo=document.createElement("td");
			var cbuttons=document.createElement("td");

			var vid = document.createTextNode(veiculos[i].id);
			var vmodelo = document.createTextNode(veiculos[i].modelo);
			var vqtdeP = document.createTextNode(veiculos[i].qtde_passageiros);
			var vativo = document.createTextNode(veiculos[i].ativo);

			var btnEdit = document.createElement('Button');
			btnEdit.setAttribute('type','button');
			btnEdit.setAttribute('class','btn btn-primary');
			btnEdit.setAttribute('id','editVeiculo');
			btnEdit.setAttribute('onclick','editarVeiculo(value)');
			btnEdit.setAttribute('data-toggle','modal');
			btnEdit.setAttribute('data-target','#exampleModal');
			btnEdit.setAttribute('value',veiculos[i].id);
			btnEdit.title = 'Editar';
			btnEdit.innerHTML = '<i class="fa fa-edit"></i>';

			var btnEx = document.createElement('Button');
			btnEx.setAttribute('type','button');
			btnEx.setAttribute('class','btn btn-danger');
			btnEx.setAttribute('id','excluirVeiculo');
			btnEx.setAttribute('onclick','excluirVeiculo(value)');
			btnEx.setAttribute('value',veiculos[i].id);
			btnEx.title = 'Excluir';
			btnEx.innerHTML = '<i class="fa fa-trash-alt"></i>';

			cid.appendChild(vid);
			cmodelo.appendChild(vmodelo);
			cqtdeP.appendChild(vqtdeP);
			cativo.appendChild(vativo);
			cbuttons.appendChild(btnEdit);
			cbuttons.appendChild(btnEx);

			linha.appendChild(cid);
			linha.appendChild(cmodelo);
			linha.appendChild(cqtdeP);
			linha.appendChild(cativo);
			linha.appendChild(cbuttons);
			tabela.appendChild(linha);
		}
	}).error(function( error ){
		console.log(error);
	});	
}

$("#atualizar-veiculo").on('click',function(){
	atualizarTabelaVeiculos();
});

$(document).ready(function(){
	
	atualizarTabelaVeiculos();
});